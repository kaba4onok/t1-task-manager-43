package ru.t1.rleonov.tm.repository;

import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    /*@Test
    public void create() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final String login = USER1.getLogin();
        @Nullable final String password = USER1.getPassword();
        @Nullable final User user = repository.create(login, password);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(password, user.getPassword());
    }

    @Test
    public void createWithEmail() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final String login = USER1.getLogin();
        @Nullable final String password = USER1.getPassword();
        @Nullable final String email = USER1.getEmail();
        @Nullable final User user = repository.create(login, password, email);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(password, user.getPassword());
        Assert.assertEquals(email, user.getEmail());
    }

    @Test
    public void createWithRole() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @Nullable final String login = USER1.getLogin();
        @Nullable final String password = USER1.getPassword();
        @Nullable final Role role = USER1.getRole();
        @Nullable final User user = repository.create(login, password, role);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(password, user.getPassword());
        Assert.assertEquals(role, user.getRole());
    }

    @Test
    public void finByLogin() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USERS);
        @Nullable final String login = USER1.getLogin();
        Assert.assertEquals(USER1, repository.findByLogin(login));
    }

    @Test
    public void finByEmail() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USERS);
        @Nullable final String email = USER1.getEmail();
        Assert.assertEquals(USER1, repository.findByEmail(email));
    }

    @Test
    public void isLoginExist() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USERS);
        @Nullable final String login = USER1.getLogin();
        Assert.assertTrue(repository.isLoginExist(login));
    }

    @Test
    public void isEmailExist() {
        @NotNull final IUserRepository repository = new UserRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USERS);
        @Nullable final String email = USER1.getEmail();
        Assert.assertTrue(repository.isEmailExist(email));
    }*/

}
