package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static ru.t1.rleonov.tm.constant.UserTestData.*;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;

public final class TaskTestData {

    /*@NotNull
    public static final TaskDTO USER1_TASK1 = new TaskDTO("USER1_TASK1", "USER1_DESC1");

    @NotNull
    public static final TaskDTO USER1_TASK2 = new TaskDTO("USER1_TASK2", "USER1_DESC2");

    @NotNull
    public static final TaskDTO USER2_TASK1 = new TaskDTO("USER2_TASK1", "USER2_DESC1");

    @NotNull
    public static final TaskDTO USER2_TASK2 = new TaskDTO("USER2_TASK2", "USER2_DESC2");

    @NotNull
    public static final TaskDTO ADMIN_TASK1 = new TaskDTO("ADMIN_TASK1", "ADMIN_DESC1");

    @NotNull
    public static final TaskDTO ADMIN_TASK2 = new TaskDTO("ADMIN_TASK2", "ADMIN_DESC2");

    @NotNull
    public static final TaskDTO WRONG_USERID_TASK = new TaskDTO("WRONG_USERID", "WRONG_USERID");

    @NotNull
    public static final List<TaskDTO> USER1_TASKS = Arrays.asList(USER1_TASK1, USER1_TASK2);

    @NotNull
    public static final List<TaskDTO> USER1_PROJECT1_TASKS = Collections.singletonList(USER1_TASK1);

    @NotNull
    public static final List<TaskDTO> USER2_TASKS = Arrays.asList(USER2_TASK1, USER2_TASK2);

    @NotNull
    public static final List<TaskDTO> ADMIN_TASKS = Arrays.asList(ADMIN_TASK1, ADMIN_TASK2);

    @NotNull
    public static final List<TaskDTO> ALL_TASKS = new ArrayList<>();

    static {
        WRONG_USERID_TASK.setUserId("!!!WRONG_USERID!!!");
        USER1_TASK1.setStatus(Status.COMPLETED);
        USER1_TASK1.setProjectId(USER1_PROJECT1.getId());
        USER1_TASK2.setProjectId(USER1_PROJECT2.getId());

        USER2_TASK1.setProjectId(USER2_PROJECT1.getId());
        USER2_TASK2.setProjectId(USER2_PROJECT2.getId());

        ADMIN_TASK1.setProjectId(ADMIN_PROJECT1.getId());
        ADMIN_TASK2.setProjectId(ADMIN_PROJECT1.getId());

        USER1_PROJECT1_TASKS.forEach(task -> task.setUserId(USER1.getId()));
        USER1_TASKS.forEach(task -> task.setUserId(USER1.getId()));
        USER2_TASKS.forEach(task -> task.setUserId(USER2.getId()));
        ADMIN_TASKS.forEach(task -> task.setUserId(ADMIN.getId()));

        ALL_TASKS.addAll(USER1_TASKS);
        ALL_TASKS.addAll(USER2_TASKS);
        ALL_TASKS.addAll(ADMIN_TASKS);
    }*/

}
