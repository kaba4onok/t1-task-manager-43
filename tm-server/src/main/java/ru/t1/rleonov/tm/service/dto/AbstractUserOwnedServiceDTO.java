package ru.t1.rleonov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.dto.IUserOwnedServiceDTO;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.rleonov.tm.enumerated.Sort;
import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO,
        R extends IUserOwnedRepositoryDTO<M>>
        extends AbstractServiceDTO<M, R>
        implements IUserOwnedServiceDTO<M> {

    public AbstractUserOwnedServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected String getSortType(@Nullable final Sort sort) {
        if (sort == Sort.BY_CREATED) return "created";
        if (sort == Sort.BY_STATUS) return "status";
        else return "name";
    }

}
