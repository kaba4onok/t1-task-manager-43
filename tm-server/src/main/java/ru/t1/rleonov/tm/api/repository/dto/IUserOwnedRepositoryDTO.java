package ru.t1.rleonov.tm.api.repository.dto;

import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends IRepositoryDTO<M> {
}
