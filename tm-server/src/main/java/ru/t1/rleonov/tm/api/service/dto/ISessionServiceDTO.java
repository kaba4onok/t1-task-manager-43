package ru.t1.rleonov.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.SessionDTO;

public interface ISessionServiceDTO extends IUserOwnedServiceDTO<SessionDTO> {

    @NotNull
    SessionDTO create(@Nullable SessionDTO session);

    Boolean existsById(@Nullable String id);

    @Nullable
    SessionDTO findOneById(@Nullable String id);

    @NotNull
    SessionDTO removeById(@Nullable String id);

}
