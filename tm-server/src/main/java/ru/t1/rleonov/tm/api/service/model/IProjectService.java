package ru.t1.rleonov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Project;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project removeById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear(@Nullable String userId);

    void clear();

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOneById(
            @Nullable String userId,
            @Nullable String id
    );

}
