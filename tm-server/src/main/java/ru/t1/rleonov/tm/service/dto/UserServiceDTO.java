package ru.t1.rleonov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.rleonov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.rleonov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.rleonov.tm.exception.entity.UserNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.exception.user.*;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import ru.t1.rleonov.tm.repository.dto.UserRepositoryDTO;
import ru.t1.rleonov.tm.util.HashUtil;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.List;

public final class UserServiceDTO extends AbstractServiceDTO<UserDTO, IUserRepositoryDTO> implements IUserServiceDTO {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectServiceDTO projectService;

    @NotNull
    private final ITaskServiceDTO taskService;

    public UserServiceDTO(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IProjectServiceDTO projectService,
            @NotNull final ITaskServiceDTO taskService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    @Override
    public IUserRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepositoryDTO(entityManager);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (hashPassword == null || hashPassword.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPassword(hashPassword);
        user.setEmail(email);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        try {
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        try {
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.clear(userId);
        projectService.clear(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        projectService.clear(userId);
        taskService.clear(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPassword(HashUtil.salt(propertyService, password));
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(user);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public void set(@NotNull final Collection<UserDTO> users) {
        if (users.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.set(users);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
