package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskService();

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IUserServiceDTO getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionServiceDTO getSessionService();

}
