package ru.t1.rleonov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;
import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}
