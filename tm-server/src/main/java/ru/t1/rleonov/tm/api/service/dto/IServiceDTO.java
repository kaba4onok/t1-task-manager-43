package ru.t1.rleonov.tm.api.service.dto;

import ru.t1.rleonov.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.rleonov.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {
}
