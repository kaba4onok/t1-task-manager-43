package ru.t1.rleonov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.model.ISessionRepository;
import ru.t1.rleonov.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull String id) {
        if (id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

}
