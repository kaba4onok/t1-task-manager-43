package ru.t1.rleonov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;
import javax.persistence.EntityManager;


public interface IUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepositoryDTO<M>, IServiceDTO<M> {

    @NotNull
    IUserOwnedRepositoryDTO<M> getRepository(@NotNull EntityManager entityManager);

}
