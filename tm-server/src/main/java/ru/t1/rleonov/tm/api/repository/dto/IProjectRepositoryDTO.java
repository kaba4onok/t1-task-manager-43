package ru.t1.rleonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import java.util.List;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId, @NotNull String sort);

    @Nullable
    ProjectDTO findOneById(@NotNull String userId, @NotNull String id);

    void clear();

    void clear(@NotNull String userId);

}
