package ru.t1.rleonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm.session")
public final class Session extends AbstractUserOwnedModel {

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

}
